package big.data.analysis.exception;

public class FlinkSubmissionException extends  RuntimeException{

    public FlinkSubmissionException(String message) {
        super(message);
    }

}
