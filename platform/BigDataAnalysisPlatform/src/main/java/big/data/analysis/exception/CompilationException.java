package big.data.analysis.exception;

public class CompilationException extends RuntimeException {

    public CompilationException(String message) {
        super(message);
    }

}
